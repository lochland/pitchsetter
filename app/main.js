// define function to return number of beats from form
window.onload = function () {
  var generator = document.getElementById("generate"), output = document.getElementById("output");
  generator.onclick = function() {
    // clear output div
    output.innerHTML = "";

    var sets = Number(document.getElementById("sets").value),
      pitches = Number(document.getElementById("pitches").value),
      equal = document.getElementById("equal").checked,
      parted = document.getElementById("parted").checked;

 //   output.innerHTML = '<div class="alert">'+sets+' '+pitches+' '+equal+' '+parted+'</span>';
    // check for fundamental errors
    try {
      if (pitches < 0) throw "The number of pitches must be between 1 and 12.";
      if (pitches > 12) throw "The number of pitches must be between 1 and 12.";
      if (sets > pitches) throw "The number of sets may not exceed the number of pitches."
    }
    catch(error) {
      output.innerHTML += '<div class="alert">'+error+'</span>';
      return false;
    }
    var pitchsets = generatePitchSet(sets, pitches, parted, equal);
    for (var i = 0; i < sets; ++i) {
      drawStave(pitchsets[i]);
    };
  };
};
