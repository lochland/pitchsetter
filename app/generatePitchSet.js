var octave = ['a', 'a#', 'b', 'c', 'c#', 'd', 'd#', 'e', 'f', 'f#', 'g', 'g#']

// props to Mike Bostock for the implementation of this algorithm; see
// https://bost.ocks.org/mike/shuffle/
function shuffle(array) {
  var m = array.length, t, i;

  // While there remain elements to shuffle…
  while (m) {

    // Pick a remaining element…
    i = Math.floor(Math.random() * m--);

    // And swap it with the current element.
    t = array[m];
    array[m] = array[i];
    array[i] = t;
  }

  return array;
}

function contains(array, item) {
  for (var i = 0; i < array.length; i++) {
    if (array[i] === item) {
      return true;
    }
  }
  return false;
}

function generatePitchSet(nrSets, nrPitches, parted, equal) {

  // check for fundamental errors
  try {
    if (nrPitches < 0) throw "Number of pitches is below zero.";
    if (nrPitches > 12) throw "Number of pitches is above twelve.";
    if (nrSets > nrPitches) throw "The number of sets cannot exceed the number of pitches."
  }
  catch(error) {
    console.log(error);
    return false;
  }

  var output = [], l;

  // check if nrPitches is divisible by nrSets
  if (equal && nrPitches % nrSets != 0) {
    // if it isn't, reduce nrPitches in shuffledOctave so that it is
    shuffledOctave = shuffle(octave).slice(0, nrPitches).slice(0, -(nrPitches % nrSets));
  } else {
    // if it is, simply return the shuffledOctave of length nrPitches
    shuffledOctave = shuffle(octave).slice(0, nrPitches);
  };

  // introduce conditions for partitioned
  if (parted) {
    // make nrSets singletons
    for (var i = 0; i < nrSets; i++) {
      output[i] = [shuffledOctave.pop()];
    }
    // assign remaining elements of shuffledOctave to sets in output
    for (var i = 0; i < shuffledOctave.length; i++) {
      // if equal is true, assign elements in a linear order
      if (equal) {
        l = i % nrSets;
      // if equal is false, assign elements at random
      } else {
        l = Math.floor(Math.random() * nrSets);
      };
      output[l].push(shuffledOctave[i]);
    };
  // conditions for not partitioned
  } else {
    // make nrSets singletons
    for (var i = 0; i < nrSets; i++) {
      pitch = shuffledOctave[Math.floor(Math.random() * nrSets)];
      output.push([pitch]);
    }

    for (var i = 0; i < nrSets; i++) {
      // conditions for equality
      if (equal) {
        l = (nrPitches / nrSets) - 1;
      } else {
        // if not equal, then set lengths are indeterminate
        l = Math.floor(Math.random() * nrPitches);
      };
      for (var j = 0; j < l; j++) {
        var randomPitch = shuffledOctave[Math.floor(Math.random() * shuffledOctave.length)];
        // if a pitch is already in one of the pitchsets, reduce the increment by one;
        // this effectively repeats the operation until a unique pitch is selected,
        // and then that pitch is pushed into the element
        if (contains(output[i], randomPitch)) {
          j--;
        } else {
          output[i].push(randomPitch);
        }
      }
    };
  }
  return output;
}
