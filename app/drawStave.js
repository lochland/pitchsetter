// makes stave using VexFlow
function drawStave(pitches) {
  VF = Vex.Flow;

  // output to output div
  var div = document.getElementById("output")
  var renderer = new VF.Renderer(div, VF.Renderer.Backends.SVG);

  // set stave formatting
  renderer.resize(500, 150);
  var context = renderer.getContext();
  context.setFont("Arial", 10, "").setBackgroundFillStyle("#eed");
  var stave = new VF.Stave(10, 40, 450);
  // set stave to percussion clef with nr beats, where nr is the bar length set below
  stave.addClef("treble");
  // draw the stave
  stave.setContext(context).draw();

  var notes = [];
  for (var i = 0; i < pitches.length; ++i) {
    if (pitches[i].length == 2) {
      notes.push(new VF.StaveNote({clef: "percussion", keys: [pitches[i]+"/4"], duration: "1" }).addAccidental(0, new VF.Accidental("#")));
    } else {
      notes.push(new VF.StaveNote({clef: "percussion", keys: [pitches[i]+"/4"], duration: "1" }));
    }
  };

  // draw notes
  Vex.Flow.Formatter.FormatAndDraw(context, stave, notes);
}
