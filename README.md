# Pitchsetter

A script for generating mode-like sets of pitches. Such pitch sets are used as a basis for improvisation, for instance.

Parameters include:

- number of pitches;
- number of sets;
- whether a given pitch can be in multiple sets;
- whether sets are all of an equal size.
